function testService(events) {
    const clients = {}
    for (const client of events) {
        if (
            (clients[client[0]] && clients[client[0]] === client[1]) ||
            (!clients[client[0]] && client[1] === 'out')
        ) {
            return false
        } else {
            clients[client[0]] = client[1]
        }

    }
    const clientsAction = Object.values(clients)
    if (clientsAction.includes('in')) {
        return false
    }
    return true
}

module.exports = testService
